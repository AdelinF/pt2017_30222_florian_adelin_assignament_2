/**
 * Created by Adelin on 04-Apr-17.
 */

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Color;

public class RunningInterface extends JFrame {

    private JPanel contentPane;
    public JTextArea textArea = new JTextArea();


    /**
     * Create the frame.
     */
    public RunningInterface() {
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 640, 480);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);


        textArea.setEditable(false);
        textArea.setBackground(Color.LIGHT_GRAY);
        textArea.setBounds(10, 11, 346, 420);
        JScrollPane scroll = new JScrollPane(textArea);
        this.add(scroll);
    }
}
