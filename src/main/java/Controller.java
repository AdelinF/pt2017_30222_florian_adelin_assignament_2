import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Adelin on 08-Apr-17.
 */
public class Controller{
    List<Queue> queues = new ArrayList();

    public void sort(){
        Collections.sort(queues);
    }

    public Queue getMinQueue(){
        Queue result = queues.get(0);
        for( int i=1; i<queues.size(); i++){
            if (queues.get(i).getWaitingTime()<result.getWaitingTime()){
                result = queues.get(i);
            }
        }
        return result;
    }
}
