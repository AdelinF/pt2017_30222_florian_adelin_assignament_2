import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * Created by Adelin on 04-Apr-17.
 */
public class Main {
    final static InputDataInterface frame1 = new InputDataInterface();
    final static RunningInterface frame2 = new RunningInterface();
    public static void main(String[] args) {
        final Controller controller = new Controller();
        final Thread t = new Thread(){
            public void run(){
                long time = System.currentTimeMillis();
                int id=1;
                while(System.currentTimeMillis()< time + (Integer)frame1.simulationTime.getValue()*1000) {
                    Random r = new Random();
                    int nextTime = r.nextInt((Integer) frame1.maxIntervalClient.getValue()) + (Integer) frame1.minIntervalClient.getValue();
                    try {
                        Thread.sleep(nextTime * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Client client = new Client();
                    client.setId(id);
                    id++;
                    client.setServiceTime(r.nextInt((Integer) frame1.maxService.getValue()) + (Integer) frame1.minService.getValue());
                    frame2.textArea.append("Client #"+client.getId()+": service time " + client.getServiceTime()+" generated after "+nextTime+"\n");
                    controller.getMinQueue().updateTime(client.getServiceTime());
                    controller.getMinQueue().add(client);
                    frame2.textArea.append("Client #"+client.getId()+": going to queue #" + controller.getMinQueue().getId()+"\n");
                    controller.sort();

                }
            }
        };

        frame1.setVisible(true);
        frame1.btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((Integer) frame1.minIntervalClient.getValue() >= (Integer) frame1.maxIntervalClient.getValue()||
                        (Integer) frame1.minService.getValue() >= (Integer) frame1.maxService.getValue()||
                        (Integer) frame1.simulationTime.getValue() <= 0 || (Integer) frame1.queuesNumber.getValue() <= 0) {
                    JOptionPane.showMessageDialog(null, "Wrong Values");
                } else {
                    for(int i=1; i<=(Integer) frame1.queuesNumber.getValue(); i++){
                        Queue q = new Queue(i);
                        q.setWaitingTime(0);
                        frame2.textArea.append("Queue #"+i+" generated!\n");
                        try{
                            Thread.sleep(3000);
                        }catch (Exception ee){
                            ee.printStackTrace();
                        }
                        Thread t2 = new Thread(q);
                        t2.start();
                        controller.queues.add(q);
                    }
                    frame2.setVisible(true);
                    t.start();
                }
            }
        });

    }

}

