import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Adelin on 04-Apr-17.
 */
public class Queue  implements Comparable<Queue>, Runnable{

    public BlockingQueue<Client> clients = new ArrayBlockingQueue(1024);
    private int id;
    private int waitingTime;
    public Queue(int id){
        this.id = id;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public void run() {
        while (true) {
            Client client = new Client();
            try{
                client = clients.take();
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                Thread.sleep(client.getServiceTime() * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.waitingTime -=client.getServiceTime();
            Main.frame2.textArea.append("Client #"+client.getId()+" has just left queue #"+this.getId()+" after "+client.getServiceTime()+"\n");
        }
    }
    public void add(Client c){
        clients.add(c);
    }

    public void updateTime(int i){
        this.waitingTime+=i;
    }

    public int getId() {
        return id;
    }


    public int getWaitingTime() {
        return waitingTime;
    }


    public int compareTo(Queue o) {
        if(waitingTime > o.getWaitingTime()){
            return -1;
        }
        if (waitingTime<o.getWaitingTime()){
            return 1;
        }else {
            return 0;
        }
    }
}
