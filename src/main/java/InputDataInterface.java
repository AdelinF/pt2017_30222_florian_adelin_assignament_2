/**
 * Created by Adelin on 04-Apr-17.
 */

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class InputDataInterface extends JFrame {

    private JPanel contentPane;
    public JSpinner minIntervalClient = new JSpinner();
    public JSpinner maxIntervalClient = new JSpinner();
    public JSpinner simulationTime = new JSpinner();
    public JSpinner minService = new JSpinner();
    public JSpinner maxService = new JSpinner();
    public JSpinner queuesNumber = new JSpinner();
    public JButton btnNewButton = new JButton("START");

    /**
     * Create the frame.
     */
    public InputDataInterface() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        panel.setBorder(new LineBorder(new Color(0, 0, 0), 5, true));
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        minIntervalClient.setBounds(183, 11, 44, 20);
        panel.add(minIntervalClient);

        JLabel lblCustomersArrivalTime = new JLabel("Customers arrival time interval:");
        lblCustomersArrivalTime.setBounds(10, 14, 187, 14);
        panel.add(lblCustomersArrivalTime);

        JLabel lblTo = new JLabel("to");
        lblTo.setBounds(237, 14, 20, 14);
        panel.add(lblTo);

        maxIntervalClient.setBounds(254, 11, 44, 20);
        panel.add(maxIntervalClient);

        JLabel lblSimulationTime = new JLabel("Simulation time:");
        lblSimulationTime.setBounds(10, 54, 177, 14);
        panel.add(lblSimulationTime);

        simulationTime.setBounds(183, 51, 44, 20);
        panel.add(simulationTime);

        JLabel lblServiceTimeInterval = new JLabel("Service time interval:");
        lblServiceTimeInterval.setBounds(10, 87, 177, 14);
        panel.add(lblServiceTimeInterval);

        minService.setBounds(183, 84, 44, 20);
        panel.add(minService);

        JLabel lblTo_1 = new JLabel("to");
        lblTo_1.setBounds(237, 87, 20, 14);
        panel.add(lblTo_1);

        maxService.setBounds(254, 84, 44, 20);
        panel.add(maxService);

        JLabel lblNumberOfQueues = new JLabel("Number of queues:");
        lblNumberOfQueues.setBounds(10, 128, 177, 14);
        panel.add(lblNumberOfQueues);

        queuesNumber.setBounds(183, 125, 44, 20);
        panel.add(queuesNumber);

        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        btnNewButton.setBounds(10, 153, 404, 88);
        panel.add(btnNewButton);
    }
}
